local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local gears = require("gears")
local themes_path = gfs.get_configuration_dir().."themes/"

local theme = {}

white               = "#f8f4d0"
orange              = "#ff7500"
orange_dark         = "#5b4738"
gray                = "#292930"
black_opaque        = "#090910b3"
opaque              = "#00000000"

theme.font          = "nimbus sans 14"
theme.tasklist_font = "nimbus sans 11"

theme.bg_normal     = black_opaque
theme.bg_focus      = black_opaque
theme.bg_urgent     = black_opaque
theme.bg_minimize   = black_opaque
theme.bg_systray    = black_opaque

theme.fg_normal     = white
theme.fg_focus      = white
theme.fg_urgent     = white
theme.fg_minimize   = white

theme.useless_gap   = dpi(8)
theme.border_width  = dpi(1)
theme.border_normal = opaque
theme.border_focus  = orange
theme.border_marked = orange

theme.notification_normal = orange

theme.tasklist_fg_normal = white
theme.tasklist_bg_normal = gray
theme.tasklist_bg_focus = white
theme.tasklist_fg_focus = gray
theme.tasklist_shape = gears.shape.transform(gears.shape.octogon)


-- 
--
theme.notification_height = 100

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."forest/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.icon_close_normal = themes_path.."forest/icons/close_normal.png"
theme.icon_close_inactive  = themes_path.."forest/icons/close_inactive.png"

theme.icon_minimize_active = themes_path.."forest/icons/minimize_active.png"
theme.icon_minimize_inactive  = themes_path.."forest/icons/minimize_inactive.png"

theme.icon_top_active = themes_path.."forest/icons/top_active.png"
theme.icon_untop_active = themes_path.."forest/icons/untop_active.png"
theme.icon_top_inactive = themes_path.."forest/icons/top_inactive.png"

theme.icon_maximize_active = themes_path.."forest/icons/maximize_active.png"
theme.icon_unmaximize_active = themes_path.."forest/icons/unmaximize_active.png"
theme.icon_maximize_inactive = themes_path.."forest/icons/maximize_inactive.png"

theme.icon_floating = themes_path.."forest/icons/floating.png"
theme.icon_tiled = themes_path.."forest/icons/tiled.png"
theme.icon_layout_inactive = themes_path.."forest/icons/layout_inactive.png"

theme.wallpaper_scr1 = themes_path.."forest/forest-wallpaper3.jpg"
theme.wallpaper_scr2 = themes_path.."forest/wallpaper-alt.png"

-- You can use your own layout icons like this:
-- theme.layout_fairh = themes_path.."forest/icons/fairhw.png"
-- theme.layout_fairv = themes_path.."forest/icons/fairvw.png"
-- theme.layout_floating  = themes_path.."forest/icons/floatingw.png"
-- theme.layout_magnifier = themes_path.."forest/icons/magnifierw.png"
-- theme.layout_max = themes_path.."forest/icons/maxw.png"
-- theme.layout_fullscreen = themes_path.."forest/icons/fullscreenw.png"
-- theme.layout_tilebottom = themes_path.."forest/icons/tilebottomw.png"
-- theme.layout_tileleft   = themes_path.."forest/icons/tileleftw.png"
-- theme.layout_tile = themes_path.."forest/icons/tilew.png"
-- theme.layout_tiletop = themes_path.."forest/icons/tiletopw.png"
-- theme.layout_spiral  = themes_path.."forest/icons/spiralw.png"
-- theme.layout_dwindle = themes_path.."forest/icons/dwindlew.png"
-- theme.layout_cornernw = themes_path.."forest/icons/cornernww.png"
-- theme.layout_cornerne = themes_path.."forest/icons/cornernew.png"
-- theme.layout_cornersw = themes_path.."forest/icons/cornersww.png"
-- theme.layout_cornerse = themes_path.."forest/icons/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme
