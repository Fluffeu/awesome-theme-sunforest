
local gears = require("gears")
local beautiful = require("beautiful")
local naughty = require("naughty")

-- ERROR HANDLING
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Awesome startup error.",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Awesome runtime error.",
                         text = tostring(err) })
        in_error = false
    end)
end
-- END ERROR HANDLING

beautiful.init(gears.filesystem.get_configuration_dir() .. "themes/forest/theme.lua")

local theme = require('sunforest.theme')
local layouts = require('sunforest.layouts')
local clients = require('sunforest.clients')
local widgets = require('sunforest.widgets')
local keybindings = require('sunforest.keybindings')
local autorun = require('sunforest.autorun')

theme.init()
layouts.init()
clients.init()
widgets.init()
keybindings.init()
autorun.init()