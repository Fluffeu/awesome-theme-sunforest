My custom AwesomeWM rice.

There are no decorators for windows, but the buttons you'd normally find in there (closing, minimizing, going fullscreen) are always available on the top bar for currently selected window. Windows on the top bar are sorted, so that currently focused window is always on the right (next to the buttons).

![Rice screenshot](screenshots/sunforest_theme_screenshot.png)
