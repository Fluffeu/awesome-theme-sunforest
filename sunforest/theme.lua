local beautiful = require("beautiful")
local gears = require("gears")
local awful = require("awful")

local function set_wallpaper(s)
	if beautiful.wallpaper_scr1 then
		local wallpaper
		if s.index == 1 then
			wallpaper = beautiful.wallpaper_scr1
		else
			wallpaper = beautiful.wallpaper_scr2
		end

		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.maximized(wallpaper, s, true)
	end
end

function init()
    awful.screen.connect_for_each_screen(function(s)
        set_wallpaper(s)
    end)
    screen.connect_signal("property::geometry", set_wallpaper)
end

return {
    init = init
}