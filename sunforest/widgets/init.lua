local awful = require("awful")
local gears = require("gears")
local menubar = require("menubar")
local wibox = require("wibox")
local beautiful = require("beautiful")
local hotkeys_popup = require("awful.hotkeys_popup")

function init()
    -- This is used later as the default terminal and editor to run.
    terminal = "alacritty"
    editor = os.getenv("EDITOR") or "nvim"
    editor_cmd = terminal .. " -e " .. editor
    
    -- {{{ Menu
    -- Create a launcher widget and a main menu
    myawesomemenu = {
       { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
       { "manual", terminal .. " -e man awesome" },
       { "edit config", editor_cmd .. " " .. awesome.conffile },
       { "restart", awesome.restart },
       { "quit", function() awesome.quit() end },
    }
    
    mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                        { "open terminal", terminal }
                                      }
                            })

    awful.screen.connect_for_each_screen(function(s)
        require("sunforest.widgets.topbar").setup_widget(s)
    end)
    
end

return {
    init = init
}
