local awful = require("awful")
local gears = require("gears")
local menubar = require("menubar")
local wibox = require("wibox")

function setup_widget(s)
    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" }, s, awful.layout.layouts[1])

    s.promptbox = awful.widget.prompt()
    s.taglist = require("sunforest.widgets.taglist").get_widget(s)
    s.tasklist = require("sunforest.widgets.tasklist").get_widget(s)
    s.clientnav = require("sunforest.widgets.clientnav").get_widget(s)

    s.textclock = wibox.widget.textclock()
    s.textclock.format = " | %R | %d.%m"

    -- Create the wibox
    s.topbar = awful.wibar({ position = "top", screen = s, height = 32 })

    s.topbar:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            {
                id = "topbar_margin",
                bg = "#00000000",
                widget = wibox.container.background,
                forced_width = 6,
            },
            s.taglist,
            s.promptbox,
        },
        {
            layout = wibox.layout.align.horizontal,
            {
                widget = wibox.container.background,
            },
            {
                widget = wibox.container.background,
            },
            s.tasklist,
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --wibox.widget.systray(),
            s.clientnav,
            s.textclock,
            {
                id = "topbar_margin",
                bg = "#00000000",
                widget = wibox.container.background,
                forced_width = 14,
            },
        },
    }
end

return {
    setup_widget = setup_widget
}