local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')
local beautiful = require("beautiful")

function get_widget(s)
  local buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
                              if client.focus then
                                  client.focus:move_to_tag(t)
                              end
                          end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
                              if client.focus then
                                  client.focus:toggle_tag(t)
                              end
                          end),
    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end))

  return 
  awful.widget.taglist {
    screen = s,
    filter  = awful.widget.taglist.filter.all,
    source = awful.widget.taglist.source.for_screen,
    buttons = buttons,
    layout = {
      spacing = 0,
      layout = wibox.layout.fixed.horizontal,
    },
    widget_template = {
      layout = wibox.layout.fixed.horizontal,
      {
        layout = wibox.layout.stack,
        {
          id = "notification_decoration",
          widget = wibox.container.background,
          shape = gears.shape.transform(gears.shape.circle):scale(0.22, 0.22):translate(80, 0),
          bg = "#00000000",
        },
        {
          layout = wibox.layout.stack,
          -- {
          --   id = "selected_decoration",
          --   bg = "#00000000",
          --   widget = wibox.container.background,
          --   shape = gears.shape.transform(gears.shape.rectangle):scale(0.6, 0.03):translate(8, 2/0.03)
          -- },
          {
            -- bottom
            id = "selected_decoration",
            bg = "#00000000",
            widget = wibox.container.background,
            shape = gears.shape.transform(gears.shape.rectangle):scale(0.9, 0.03):translate(2, 23/0.03),
          },
          {
            -- up
            id = "selected_decoration",
            bg = "#00000000",
            widget = wibox.container.background,
            shape = gears.shape.transform(gears.shape.rectangle):scale(0.9, 0.03):translate(2, 2/0.03),
          },
          {
            -- right
            id = "selected_decoration",
            bg = "#00000000",
            widget = wibox.container.background,
            shape = gears.shape.transform(gears.shape.rectangle):scale(0.05, 0.65):translate(22/0.05, 24*0.1875),
          },
          {
            -- left
            id = "selected_decoration",
            bg = "#00000000",
            widget = wibox.container.background,
            shape = gears.shape.transform(gears.shape.rectangle):scale(0.05, 0.65):translate(10, 24*0.1875),
          },
        },
        {
          id = "nonempty_decoration",
          widget = wibox.container.background,
          shape = gears.shape.transform(gears.shape.rectangle):scale(0.6, 0.03):translate(8, 23/0.03),
          bg = "#00000000",
        },
        {
          id = "text_role",
          widget = wibox.widget.textbox,
          align = "center",
          valign = "center",
          bg = beautiful.fg_normal,
          forced_width = 24,
        },
      },
      update_callback = function(self, tag, index, objects)
        selected_decorations = self:get_children_by_id('selected_decoration')
        if tag.selected then
          for i = 1, #selected_decorations do
            selected_decorations[i].bg = beautiful.fg_normal
          end
        else
          for i = 1, #selected_decorations do
            selected_decorations[i].bg = "#00000000"
          end
        end
  
        local nonempty = #tag:clients() > 0
        if nonempty then
          self:get_children_by_id('nonempty_decoration')[1].bg = beautiful.fg_normal
        else
          self:get_children_by_id('nonempty_decoration')[1].bg = "#00000000"
        end

        if tag.urgent then
          self:get_children_by_id('notification_decoration')[1].bg = beautiful.notification_normal
        else
          self:get_children_by_id('notification_decoration')[1].bg = "#00000000"
        end
      end
    },
  }
end

return {
  get_widget = get_widget,
}
