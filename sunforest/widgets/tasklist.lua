
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")

function get_widget(screen)
    buttons = gears.table.join(
        awful.button({ }, 1, function (c)
                                if c == client.focus then
                                    c.minimized = true
                                else
                                    c:emit_signal(
                                        "request::activate",
                                        "tasklist",
                                        {raise = true}
                                    )
                                end
                            end),
        awful.button({ }, 3, function()
                                awful.menu.client_list({ theme = { width = 250 } })
                            end),
        awful.button({ }, 4, function ()
                                awful.client.focus.byidx(1)
                            end),
        awful.button({ }, 5, function ()
                                awful.client.focus.byidx(-1)
                            end))

    return awful.widget.tasklist {
        screen = screen,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = buttons,
        layout = {
            spacing = 10,
            layout = wibox.layout.flex.horizontal,
        },
        widget_template = {
            id = "test123",
            widget = wibox.container.background,
            {
                widget = wibox.container.margin,
                top = 5,
                bottom = 5,
                {
                    layout = wibox.layout.stack,
                    {
                        id = "background_role",
                        widget = wibox.container.background,
                        bg = beautiful.bg_tasklist,
                    },
                    {
                        layout = wibox.layout.fixed.horizontal,
                        {
                            widget  = wibox.container.margin,
                            {
                                id = "clienticon",
                                widget = awful.widget.clienticon,
                            },
                        },
                        {
                            id = "text_role",
                            widget = wibox.widget.textbox,
                            forced_width = 200,
                        },
                    },
                },
            },
        },
        source = function()
            -- TODO: make it, so that focusing client always swaps it with previously focused one
            -- TODO: right clicking tasklist item to minimize window
            -- TODO: sorting clients, so that minimized ones are earlier (on the left)
            all_clients = awful.widget.tasklist.source.all_clients()
            for i = 1, #all_clients do
                if all_clients[i] == client.focus then
                    all_clients[i] = all_clients[#all_clients]
                    all_clients[#all_clients] = client.focus
                end
            end
            return all_clients
        end
    }
end

return {
    get_widget = get_widget
}