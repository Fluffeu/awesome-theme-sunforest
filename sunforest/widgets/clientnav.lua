local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')
local beautiful = require("beautiful")

function create_close_button()
    local focus_state = true

    local actions = gears.table.join(
        awful.button({ }, 1, 
        function()
            if focus_state then
                client.focus:kill()
            end
        end)
    )

    local button = wibox.widget {
        image  = beautiful.icon_close_normal,
        resize = true,
        widget = wibox.widget.imagebox,
        buttons = actions,
        update_icon = function(self)
            icon_to_set = beautiful.icon_close_normal
            if focus_state == false then
                icon_to_set = beautiful.icon_close_inactive
            end
            if image ~= icon_to_set then
                self:set_image(icon_to_set)
            end
        end,
    }

    client.connect_signal('focus', function(c)
        focus_state = true
        button:update_icon(button)
    end)
    
    client.connect_signal('unfocus', function(c)
        focus_state = false
        awful.spawn.easy_async("sleep 0.1", function()
            button:update_icon(button)
        end)
    end)

    return button
end

function create_top_button()
    local focus_state = true
    local ontop_state = false

    local actions = gears.table.join(
        awful.button({ }, 1, 
        function()
            if focus_state then
                client.focus.ontop = not client.focus.ontop
            end
        end)
    )

    local button = wibox.widget {
        image  = beautiful.icon_top_active,
        resize = true,
        widget = wibox.widget.imagebox,
        buttons = actions,
        update_icon = function(self)
            icon_to_set = beautiful.icon_top_active
            if focus_state == false then
                icon_to_set = beautiful.icon_top_inactive
            else
                if ontop_state then
                    icon_to_set = beautiful.icon_untop_active
                end
            end
            if image ~= icon_to_set then
                self:set_image(icon_to_set)
            end
        end,
    }

    client.connect_signal('focus', function(c)
        focus_state = true
        ontop_state = c.ontop
        button:update_icon(button)
    end)
    
    client.connect_signal('unfocus', function(c)
        focus_state = false
        awful.spawn.easy_async("sleep 0.1", function()
            button:update_icon(button)
        end)
    end)

    client.connect_signal("property::ontop", function(c)
        ontop_state = c.ontop
        button:update_icon(button)
    end)

    return button
end

function create_maximize_button()
    local focus_state = true
    local maximize_state = false

    local actions = gears.table.join(
        awful.button({ }, 1, 
        function()
            if focus_state then
                client.focus.maximized = not client.focus.maximized
                client.focus:raise()
            end
        end)
    )

    local button = wibox.widget {
        image  = beautiful.icon_maximize_active,
        resize = true,
        widget = wibox.widget.imagebox,
        buttons = actions,
        update_icon = function(self)
            icon_to_set = beautiful.icon_maximize_active
            if focus_state == false then
                icon_to_set = beautiful.icon_maximize_inactive
            else
                if maximize_state then
                    icon_to_set = beautiful.icon_unmaximize_active
                end
            end
            if image ~= icon_to_set then
                self:set_image(icon_to_set)
            end
        end,
    }

    client.connect_signal('focus', function(c)
        focus_state = true
        maximize_state = c.maximized
        button:update_icon(button)
    end)
    
    client.connect_signal('unfocus', function(c)
        focus_state = false
        awful.spawn.easy_async("sleep 0.1", function()
            button:update_icon(button)
        end)
    end)

    client.connect_signal("property::maximized", function(c)
        maximize_state = c.maximized
        button:update_icon(button)
    end)

    return button
end

function create_float_button()
    local focus_state = true
    local float_state = false

    local actions = gears.table.join(
        awful.button({ }, 1, 
        function()
            if focus_state then
                client.focus.floating = not client.focus.floating
                client.focus:raise()
            end
        end)
    )

    local button = wibox.widget {
        image  = beautiful.icon_floating,
        resize = true,
        widget = wibox.widget.imagebox,
        buttons = actions,
        update_icon = function(self)
            icon_to_set = beautiful.icon_floating
            if focus_state == false then
                icon_to_set = beautiful.icon_layout_inactive
            else
                if float_state then
                    icon_to_set = beautiful.icon_tiled
                end
            end
            if image ~= icon_to_set then
                self:set_image(icon_to_set)
            end
        end,
    }

    client.connect_signal('focus', function(c)
        focus_state = true
        float_state = c.floating
        button:update_icon(button)
    end)
    
    client.connect_signal('unfocus', function(c)
        focus_state = false
        awful.spawn.easy_async("sleep 0.1", function()
            button:update_icon(button)
        end)
    end)

    client.connect_signal("property::floating", function(c)
        float_state = c.floating
        button:update_icon(button)
    end)

    return button
end

function get_widget(s)
    return
    {
        layout = wibox.layout.fixed.horizontal,
        create_float_button(),
        create_maximize_button(),
        create_top_button(),
        create_close_button(),
    }
end

return {
  get_widget = get_widget,
}
