local naughty = require("naughty")

function print_debug(text)
    naughty.notify({ preset = naughty.config.presets.critical,
                    title = "Debug",
                    text = tostring(text) })
end


return {
    print_debug = print_debug
}
