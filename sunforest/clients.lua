local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local keybindings = require("sunforest.keybindings")
local naughty = require("naughty")

require("awful.autofocus")

function create_rules()
    awful.rules.rules = {
        -- Defaults
        { rule = {},
        properties = { border_width = beautiful.border_width,
                        border_color = beautiful.border_normal,
                        focus = awful.client.focus.filter,
                        raise = true,
                        keys = keybindings.get_client_keys,
                        buttons = keybindings.get_client_buttons,
                        screen = awful.screen.preferred,
                        placement = awful.placement.no_overlap+awful.placement.no_offscreen
                    }
        },
        -- Add titlebars to normal clients and dialogs
        -- { rule_any = {type = { "normal", "dialog" }
        -- }, properties = { titlebars_enabled = true }
        -- },
        { rule_any = { class = { "Firefox", "firefox", "librewolf", "LibreWolf" } }, properties = { screen = 2, tag = "1" } },
        { rule_any = { class = { "Lutris", "lutris" } }, properties = { screen = 1, tag = "3" } },
        { rule_any = { class = { "Spotify", "spotify", "Discord", "discord" }, name = { "Spotify", "spotify" } }, properties = { screen = 2, tag = "0" } },
        { rule_any = { class = { "telegram-desktop", "Telegram", "TelegramDesktop" } }, properties = { screen = 2, tag = "8" } },
        { rule_any = { class = { "pavucontrol", "Pavucontrol" } }, properties = { screen = 1, tag = "6" } },
        { rule_any = { class = { "league of legends.exe", "leagueclient.exe", "riotclientux.exe" } }, properties = {screen = 1, tag = "2" } },
        { rule_any = { class = { "explorer.exe" }, name = { "Wine System Tray", "wine system tray" }}, properties = { hidden = true, below = true, focusable = false, width = 0, height = 0 } },
    }
end

function init()
    create_rules()

    -- Signal function to execute when a new client appears.
    client.connect_signal("manage", function (c)
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- if not awesome.startup then awful.client.setslave(c) end

        -- naughty.notify({ preset = naughty.config.presets.critical,
        --     title = "Spawning new window",
        --     text = "class: "..c.class})
        if c.class == nil then

        end

        if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
            -- Prevent clients from being unreachable after screen count changes.
            awful.placement.no_offscreen(c)
        end
    end)

    -- Add a titlebar if titlebars_enabled is set to true in the rules.
    client.connect_signal("request::titlebars", function(c)
        -- buttons for the titlebar
        local buttons = gears.table.join(
            awful.button({ }, 1, function()
                c:emit_signal("request::activate", "titlebar", {raise = true})
                awful.mouse.client.move(c)
            end),
            awful.button({ }, 3, function()
                c:emit_signal("request::activate", "titlebar", {raise = true})
                awful.mouse.client.resize(c)
            end)
        )

        -- awful.titlebar(c) : setup {
        --    { -- Left
        --        awful.titlebar.widget.iconwidget(c),
        --        buttons = buttons,
    --            layout  = wibox.layout.fixed.horizontal
    --        },
    --        { -- Middle
    --            { -- Title
    --                align  = "center",
    --                widget = awful.titlebar.widget.titlewidget(c)
    --            },
    --            buttons = buttons,
    --            layout  = wibox.layout.flex.horizontal
    --        },
    --        { -- Right
    --            awful.titlebar.widget.floatingbutton (c),
    --            awful.titlebar.widget.maximizedbutton(c),
    --            awful.titlebar.widget.stickybutton   (c),
    --            awful.titlebar.widget.ontopbutton    (c),
    --            awful.titlebar.widget.closebutton    (c),
    --            layout = wibox.layout.fixed.horizontal()
    --        },
    --        layout = wibox.layout.align.horizontal
    --    }
    end)

    client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
    client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
end

return {
    init = init
}