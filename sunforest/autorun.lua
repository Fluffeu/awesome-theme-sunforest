local awful = require("awful")
local naughty = require("naughty")

-- Run the program if it's not running currently
function spawn_once(app_name)
    awful.spawn.easy_async_with_shell("ps aux | grep "..app_name.." | grep -v grep > /tmp/run_check_"..app_name, function()
        awful.spawn.easy_async_with_shell("cat /tmp/run_check_"..app_name, function(out)
            if string.len(out) == 0 then
                awful.util.spawn(app_name)
            end
        end)
    end)
end

function init()
    autorun_apps =
    {
        "xrandr --output HDMI-A-1 --rotate right",
        "picom",
        "pipewire-session-start",
        "solaar --window hide",
    }
    for i = 1, #autorun_apps do
        spawn_once(autorun_apps[i])
    end
end

return {
    init = init
}
